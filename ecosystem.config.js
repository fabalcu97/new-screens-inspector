module.exports = {
  apps : [/* {
    name        : "content-server",
    script      : "./src/core/content-server.js",
    watch       : false
  }, {
    name        : "displays-chromecast",
    script      : "./src/core/displays-chromecast.js",
    watch       : false
  }, */ {
    name        : "bluetooth-synchronization",
    script      : "./src/core/bluetooth-synchronization.js",
    watch       : true
  }]
}