let bleno = require('bleno');
let utils = require('../utils');
var Characteristic = bleno.Characteristic;

var WriteCharacteristic = new Characteristic({
    uuid: utils.characteristics.writeUUID,
    properties: [ 'write' ],
    secure: [ 'write' ],
    value: null,
    descriptors: [],
    onWriteRequest: (data, offset, withoutResponse, callback) => {
      let message = utils.bytesToString(data);
      console.log('MSG => ', message);
    },
});

module.exports = WriteCharacteristic;