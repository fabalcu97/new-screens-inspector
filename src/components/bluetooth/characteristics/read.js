let bleno = require('bleno');
let utils = require('../utils');

var Characteristic = bleno.Characteristic;

var ReadCharacteristic = new Characteristic({
    uuid: utils.characteristics.readUUID,
    properties: [ 'read' ],
    secure: [ 'read' ],
    value: null,
    descriptors: [],
    onReadRequest: (offset, callback) => {
      var result = Characteristic.RESULT_SUCCESS;
      var data = utils.stringToBytes("Hello Back!");

      console.log("Reading message...");

      callback(result, data);
    },
});

module.exports = ReadCharacteristic;