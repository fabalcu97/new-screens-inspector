let bleno = require('bleno');
let shelljs = require('shelljs');
let utils = require('../components/bluetooth/utils');
let BlenoPrimaryService = bleno.PrimaryService;

let writeCharacteristic = require('../components/bluetooth/characteristics/write');
let readCharacteristic = require('../components/bluetooth/characteristics/read');
let echoCharacteristic = require('../components/bluetooth/characteristics/echo');

let name = 'Screens Inspector';

// shelljs.exec('sudo rfkill unblock all');
shelljs.exec('sudo hciconfig hci0 up');
shelljs.exec('sudo bluetoothctl <<EOF pairable on EOF');

bleno.on('advertisingStartError', (err) => {
  console.log('Adv Error =>', err);
});

bleno.on('advertisingStart', (err) => {
  console.log('Advertising started');
  if (err){
    console.log('AdvError =>', err);
  }
  if (!err) {
    bleno.setServices([
      new BlenoPrimaryService({
        uuid: 'ec00',
        characteristics: [
          new echoCharacteristic()
        ]
      })
      // new BlenoPrimaryService({
      //   uuid: utils.serviceUUID,
      //   characteristics: [
      //     readCharacteristic,
      //     writeCharacteristic
      //   ]
      // })
    ]);
  }
});

bleno.on('servicesSetError', (err) => {
  console.log('Error setting services =>', err);
});

bleno.on('accept', (clientAddress) => {
  console.log('Connected to =>', clientAddress);
});

bleno.on('disconnect', (clientAddress) => {
  console.log('Disconnected from =>', clientAddress);
})

bleno.on('stateChange', (state) => {
  console.log('State => ', state);
  if (state == 'poweredOn') {
    bleno.startAdvertising(name, ['ec00'], (err) => {
      if (err) console.log('Adv Error =>', err);
    });
  }
});