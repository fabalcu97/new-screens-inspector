var FileBucketServer = require('files-bucket-server');
var fBServer = new FileBucketServer('./files', { logsEnabled: true });
 
// Only allow local requests
fBServer.onlyAllowLocalRequests();
 
// Start server
fBServer.start().then(function (serverData) {
    console.log('Server is up at port: '+serverData.port);
});